#---------------------------------------------------------------------
# Function: InstallBind
#    Install bind DNS server
#---------------------------------------------------------------------
InstallBind() {
  echo -n "Installing Bind9... ";
  apt-get -yqq install bind9 dnsutils > /dev/null 2>&1


  cat > /etc/bind/named.conf.options <<EOF
options {
        directory "/var/cache/bind";

        // If there is a firewall between you and nameservers you want
        // to talk to, you may need to fix the firewall to allow multiple
        // ports to talk.  See http://www.kb.cert.org/vuls/id/800113

        // If your ISP provided one or more IP addresses for stable
        // nameservers, you probably want to use them as forwarders.
        // Uncomment the following block, and insert the addresses replacing
        // the all-0's placeholder.

        forwarders {
            8.8.8.8;
            8.8.4.4;
        };

        //========================================================================
        // If BIND logs error messages about the root key being expired,
        // you will need to update your keys.  See https://www.isc.org/bind-keys
        //========================================================================
        dnssec-enable yes;
        dnssec-validation yes;
        dnssec-lookaside auto;

        auth-nxdomain no;    # conform to RFC1035
        listen-on-v6 { any; };
};

EOF

  /etc/init.d/bind9 restart
  echo -e "[${green}DONE${NC}]\n"
}