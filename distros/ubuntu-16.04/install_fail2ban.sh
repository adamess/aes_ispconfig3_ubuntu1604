#---------------------------------------------------------------------
# Function: InstallFail2ban
#    Install and configure fail2ban and UFW
#---------------------------------------------------------------------
InstallFail2ban() {
  echo -n "Installing fail2ban... "
  apt-get -yqq install fail2ban > /dev/null 2>&1
  echo -n "Installing Ubuntu Firewall... "
  apt-get -yqq install ufw

cat > /etc/fail2ban/jail.local <<EOF
# Webess Configured File

[INCLUDES]

before = paths-debian.conf

[DEFAULT]

#
# MISCELLANEOUS OPTIONS
#

ignoreip = 127.0.0.1/8

ignorecommand =

bantime  = 1500
findtime  = 750
maxretry = 6

backend = auto
usedns = warn
logencoding = auto

# "enabled" enables ALL the jails.
# true:  jail will be enabled and log files will get monitored for changes
# false: jail is not enabled
enabled = false  


# "filter" defines the filter to use by the jail.
#  By default jails have names matching their filter name
#
filter = %(__name__)s

#
# ACTIONS
#

destemail = root@localhost

# Sender email address used solely for some actions
sender = root@localhost

mta = sendmail

# Default protocol
protocol = tcp
chain = INPUT
port = 0:65535

banaction = iptables-multiport

# The simplest action to take: ban only
action_ = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]

# ban & send an e-mail with whois report to the destemail.
action_mw = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
            %(mta)s-whois[name=%(__name__)s, dest="%(destemail)s", protocol="%(protocol)s", chain="%(chain)s"]

# ban & send an e-mail with whois report and relevant log lines
# to the destemail.
action_mwl = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
             %(mta)s-whois-lines[name=%(__name__)s, dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s"]

# See the IMPORTANT note in action.d/xarf-login-attack for when to use this action
#
# ban & send a xarf e-mail to abuse contact of IP address and include relevant log lines
# to the destemail.
action_xarf = %(banaction)s[name=%(__name__)s, bantime="%(bantime)s", port="%(port)s", protocol="%(protocol)s", chain="%(chain)s"]
             xarf-login-attack[service=%(__name__)s, sender="%(sender)s", logpath=%(logpath)s, port="%(port)s"]

# ban IP on CloudFlare & send an e-mail with whois report and relevant log lines
# to the destemail.
action_cf_mwl = cloudflare[cfuser="%(cfemail)s", cftoken="%(cfapikey)s"]
                %(mta)s-whois-lines[name=%(__name__)s, dest="%(destemail)s", logpath=%(logpath)s, chain="%(chain)s"]

action_blocklist_de  = blocklist_de[email="%(sender)s", service=%(filter)s, apikey="%(blocklist_de_apikey)s"]

action_badips = badips.py[category="%(name)s", banaction="%(banaction)s"]

action = %(action_)s


#
# JAILS
#

#
# SSH servers
#

[sshd]
enabled  = true
port    = ssh
logpath = %(sshd_log)s


[sshd-ddos]
enabled = true
port    = ssh
logpath = %(sshd_log)s


[dropbear]
enabled = false
port     = ssh
logpath  = %(dropbear_log)s


[selinux-ssh]
enabled = false
port     = ssh
logpath  = %(auditd_log)s
maxretry = 5


#
# HTTP servers
#

[nginx-http-auth]
enabled  = true
port    = http,https
logpath = %(nginx_error_log)s

[nginx-botsearch]
enabled = true
port     = http,https
logpath  = %(nginx_error_log)s
maxretry = 6

# Ban attackers that try to use PHP's URL-fopen() functionality
# through GET/POST variables. - Experimental, with more than a year
# of usage in production environments.

[php-url-fopen]
enabled = false
port    = http,https
logpath = %(nginx_access_log)s


#
# FTP servers
#

[proftpd]
enabled = false
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(proftpd_log)s

[pureftpd]
enabled = true
port = ftp
filter = pureftpd
logpath = /var/log/syslog
maxretry = 3

[gssftpd]
enabled = false
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(syslog_daemon)s
maxretry = 6

[wuftpd]
enabled = false
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(wuftpd_log)s
maxretry = 6

[vsftpd]
enabled = false
port     = ftp,ftp-data,ftps,ftps-data
logpath  = %(vsftpd_log)s
# logpath = %(syslog_authpriv)s

#
# Miscellaneous
#

[asterisk]
enabled = false
port     = 5060,5061
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol="tcp", chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol="udp", chain="%(chain)s", actname=%(banaction)s-udp]
           %(mta)s-whois[name=%(__name__)s, dest="%(destemail)s"]
logpath  = /var/log/asterisk/messages
maxretry = 10


[freeswitch]
enabled = false
port     = 5060,5061
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol="tcp", chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol="udp", chain="%(chain)s", actname=%(banaction)s-udp]
           %(mta)s-whois[name=%(__name__)s, dest="%(destemail)s"]
logpath  = /var/log/freeswitch.log
maxretry = 10


# To log wrong MySQL access attempts add to /etc/my.cnf in [mysqld] or
# equivalent section:
# log-warning = 2
#
# for syslog (daemon facility)
# [mysqld_safe]
# syslog
#
# for own logfile
[mysqld]
log-error=/var/log/mysql.log
[mysqld-auth]
enabled  = false
port     = 3306
logpath  = %(mysql_log)s
maxretry = 10


# Jail for more extended banning of persistent abusers
# !!! WARNINGS !!!
# 1. Make sure that your loglevel specified in fail2ban.conf/.local
#    is not at DEBUG level -- which might then cause fail2ban to fall into
#    an infinite loop constantly feeding itself with non-informative lines
# 2. Increase dbpurgeage defined in fail2ban.conf to e.g. 648000 (7.5 days)
#    to maintain entries for failed logins for sufficient amount of time
[recidive]
enabled  = true
logpath  = /var/log/fail2ban.log
banaction = iptables-allports
bantime  = 604800  ; 1 week
findtime = 86400   ; 1 day
maxretry = 5


# Generic filter for PAM. Has to be used with action which bans all
# ports such as iptables-allports, shorewall

[pam-generic]
enabled = true
# pam-generic filter can be customized to monitor specific subset of 'tty's
banaction = iptables-allports
logpath  = %(syslog_authpriv)s


[xinetd-fail]
enabled = false
banaction = iptables-multiport-log
logpath   = %(syslog_daemon)s
maxretry  = 2


# stunnel - need to set port for this
[stunnel]
enabled = false
logpath = /var/log/stunnel4/stunnel.log


[ejabberd-auth]
enabled = false
port    = 5222
logpath = /var/log/ejabberd/ejabberd.log

[directadmin]
enabled = false
logpath = /var/log/directadmin/login.log
port = 2222

[portsentry]
enabled  = false
logpath  = /var/lib/portsentry/portsentry.history
maxretry = 1

[pass2allow-ftp]
enabled = false
# this pass2allow example allows FTP traffic after successful HTTP authentication
port         = ftp,ftp-data,ftps,ftps-data
# knocking_url variable must be overridden to some secret value in filter.d/apache-pass.local
filter       = apache-pass
# access log of the website with HTTP auth
logpath      = %(apache_access_log)s
blocktype    = RETURN
returntype   = DROP
bantime      = 3600
maxretry     = 1
findtime     = 1

EOF

cat > /etc/fail2ban/filter.d/pureftpd.conf <<EOF
[Definition]
failregex = .*pure-ftpd: \(.*@<HOST>\) \[WARNING\] Authentication failed for user.*
ignoreregex =
EOF

  service fail2ban restart > /dev/null 2>&1
  echo -e "[${green}DONE${NC}]\n"
}

