#---------------------------------------------------------------------
# Function: InstallWPcli
#    Do some pre-install checks
#---------------------------------------------------------------------
InstallWPcli () {
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
php wp-cli.phar --info
echo "Does the wp-cli.phar version infor look correct? if not please install manually"
read DUMMY
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
}
