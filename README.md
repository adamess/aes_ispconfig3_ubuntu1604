# README #

# Version #
v.1.0

This is a system to automate the installation of ISPConfig 3 control Panel ( http://www.ispconfig.org/page/home.html ).

Tested on:

- Ubuntu 16.04 Xenial Xerus
- ISPConfig 3.*

### What is this repository for? ###

This repository contains some scripts for the automation

of installation of ISPConfig 3 control panel.

You can Choose during install:
- Apache / Nginx
- Dovecot or Courier
- Quota On/Off
- Jailkit On/Off
- Squirrelmail / Roundcube
- ISPConfig 3 Standard / Expert mode

### How do I get set up? ###

* Summary of set up

```shell
cd /tmp; git clone "https://adamess@bitbucket.org/adamess/aes_ispconfig3_ubuntu1604.git"; cd *ispconfig*; bash install.sh
```

Follow the instruction on the screen

### Who had contributed to this work? ###

* The some scripts and instructions have been produced by Adam Ess ( <adam@adamess.me> )
* Special thanks to Torsten Widmann for contribution to the code
* The code is based on the "Automatic Debian System Installation for ISPConfig 3" of Author: Mark Stunnenberg <mark@e-rave.nl>
* The code is also based on the work by Matteo Temporini ( <temporini.matteo@gmail.com> )
* Howtoforge community https://www.howtoforge.com/community/
